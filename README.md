# Loki

![Ghost version](https://img.shields.io/badge/Ghost-0.9.x-brightgreen.svg?style=flat-square)

> A simple dark theme for Ghost, a combination of Odin and Casper Shadow Front.

## Intro
**Loki** is a very simple fork of the Ghost custom theme [Odin](https://github.com/h4t0n/odin), and incorporates the
dark theme of [Casper Shadow Front](https://github.com/vvision/Casper-Shadow-Front).

Seriously, please go to these other projects and donate to them! I just merged the two things together. I also turned
the links red, because I really like red links in darker themes.

### Features
* Casper minimalistic and clean style (without right side menu)
* Works with Ghost 0.8+
* Fully responsive (for mobiles and tablets)
* Home Page Navigation Menu Buttons
* Google Analytics (easily configurable by code injection in the admin area)
* [Disqus](https://disqus.com) comments (easily configurable by code injection in the admin area)
* [Prism](http://prismjs.com/) Syntax Highlight (all languages supported)
* [RRSSB](https://github.com/kni-labs/rrssb) Extraordinary Social Sharing Buttons
* [Font Awesome](http://fontawesome.io) home page Social Link Icons (easily configurable by code injection in the admin area)

### Sample
I've got a sample of the dark theme at [ignisphaseone.com](https://ignisphaseone.com).

## Installation
Installation is the same as other themes, so clone or download the content of this repo inside your Ghost `content/themes/` folder.

```
# for example
$ cd /your-ghost-root-directory
$ git clone https://bitbucket.org/ignisphaseone/loki.git content/themes/loki
```

Restart Ghost and select Loki theme from your Admin Area.

## Configuration
No need to configure ***Prism*** or ***RRSSB*** buttons.

To add Homepage Navigation Menu Buttons simply add the links in your Navigation Admin Area. They may be useful for static pages (*AboutMe* for example) or for shortcut to your (best) post tags.  

Loki comes with my default ***favicon***, mostly because I don't know how to generate a favicon that is generic enough for general use, and I think it looks cool. If you want to add your *favicon* you can generate your own (with [Real Favicon Generator](http://realfavicongenerator.net)) and place downloaded files inside the ***assets/img/favicons*** Loki directory.

***Disqus*** comments, ***Google Analytics***  and ***Font Awesome Home Page Social Link Icons*** are disabled by default, but they are easily configurable with *Blog Header Code Injection* inside your Ghost Admin Area.

```html
<script>
// to enable Google Analytics
var ga_id = 'YOUR-UA-ID_HERE';

// to enable Disqus
var disqus_shortname = 'YOUR_DISQUS_SHORTNAME'

// to enable Social Link Icons add the social_link object
// with the pair key/value -> social_network/link
// NB: the key is used to include the right icon from Font Awesome
// (you can include any Font Awesome icon)

// Example: squared social network icons
var social_link = {
    'twitter-square': 'https://twitter.com/ignisphase1',
    'bitbucket-square': 'https://bitbucket.org/ignisphaseone',
    'rss':'https://ignisphaseone.com/rss/'
    // you can add more icons
}

</script>
```

## Copyright & License
Released under the MIT License.
Copyright (c) 2016 [Eric Fong](https://ignisphaseone.com)
Copyright (c) 2016 [Andrea Tarquini](https://blog.h4t0n.com) aka [@h4ton](https://twitter.com/h4t0n) (for Odin additions)
Copyright (c) 2013-2015 [Victor Voisin](https://github.com/vvision/Casper-Shadow-Front) (for dark color values from Casper Shadow Front)
Copyright (c) 2013-2015 Ghost Foundation (for Casper theme substantial portions of code)
